package de.knorrbremse.kip.viewmodel;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.OnLifecycleEvent;
import android.arch.lifecycle.ViewModel;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public abstract class BaseViewModel extends ViewModel {

    private CompositeDisposable disposables = new CompositeDisposable();

    public void addDisposable(Disposable disposable) {
        disposables.add(disposable);
    }

    public void removeDisposable(Disposable disposable) {
        disposables.remove(disposable);
    }

    public void clearDisposables() {
        disposables.dispose();
        disposables.clear();
    }

    @Override
    protected void onCleared() {
        super.onCleared();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public void onDestroy() {
        clearDisposables();
    }


}
