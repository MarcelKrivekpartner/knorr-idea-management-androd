package de.knorrbremse.kip.di;

import dagger.Module;

@SuppressWarnings("unused")
@Module
public abstract class ViewModelModule {
}
