package de.knorrbremse.kip.di;

import dagger.Module;

@Module(includes = {ViewModelModule.class})
public class AppModule {
}
