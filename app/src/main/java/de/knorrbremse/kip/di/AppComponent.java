package de.knorrbremse.kip.di;
import dagger.Component;
import dagger.android.support.AndroidSupportInjectionModule;
import de.knorrbremse.kip.KipApp;

@ApplicationScope
@Component(
        modules = {
                AndroidSupportInjectionModule.class,
                AppModule.class,
                MainActivityModule.class}
)
public interface AppComponent {

    void inject(KipApp kipApp);
}
