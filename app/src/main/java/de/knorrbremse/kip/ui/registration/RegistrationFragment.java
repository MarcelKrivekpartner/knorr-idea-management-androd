package de.knorrbremse.kip.ui.registration;


import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;

import com.jakewharton.rxbinding2.view.RxView;

import androidx.navigation.Navigation;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.knorrbremse.kip.R;
import io.reactivex.disposables.Disposable;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegistrationFragment extends Fragment {

    @BindView(R.id.btn_register)
    Button btnRegister;

    @BindView(R.id.et_email)
    TextInputEditText etEmail;

    @BindView(R.id.et_personal_number)
    TextInputEditText etPersonalNumber;

    @BindView(R.id.et_password)
    TextInputEditText etPassword;

    @BindView(R.id.et_repeat_password)
    TextInputEditText etRepeatPassword;

    @BindView(R.id.til_repeat_password)
    TextInputLayout tilRepeatPassword;

    @BindView(R.id.ib_show_password)
    ImageButton ibShowPassword;

    @BindView(R.id.ib_show_repeat_password)
    ImageButton ibShowRepeatPassword;

    @BindView(R.id.cb_agb)
    CheckBox cbAgb;

    @BindView(R.id.tv_terms_conditions)
    TextView tvTermsConditions;

    private boolean showPassword = false;
    private boolean showRepeatPassword = false;

    public RegistrationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_registration, container, false);
    }

    @SuppressLint("CheckResult")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        RxView.clicks(btnRegister).subscribe(o -> {
            boolean register = true;
            if (etEmail.getText().toString().equals("@knorrbremse.de")) {
                register = false;
                etEmail.setError(getResources().getString(R.string.email_not_empty));
            }
            if (TextUtils.isEmpty(etPersonalNumber.getText())) {
                register = false;
                etPersonalNumber.setError(getResources().getString(R.string.personal_number_not_empty));
            }
            if (TextUtils.isEmpty(etPassword.getText())) {
                register = false;
                etPassword.setError(getResources().getString(R.string.password_not_empty));
            }
            if (TextUtils.isEmpty(etRepeatPassword.getText()) ||
                    (!TextUtils.isEmpty(etPassword.getText()) &&
                            !etRepeatPassword.getText().toString().equals(etPassword.getText().toString()))) {
                register = false;
                etRepeatPassword.setError(getResources().getString(R.string.passwords_must_match));
            }
            if (!cbAgb.isChecked()) {
                register = false;
                tvTermsConditions.setTextColor(getResources().getColor(R.color.red));
            }
            if (register)
                Navigation.findNavController(view).popBackStack();
        });
        RxView.clicks(ibShowPassword).subscribe(o -> {
            if (!showPassword) {
                etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                ibShowPassword.setImageResource(R.drawable.ic_visibility_off_blue_22dp);
                showPassword = true;
            } else {
                etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                ibShowPassword.setImageResource(R.drawable.ic_visibility_blue_22dp);
                showPassword = false;
            }
        });
        RxView.clicks(ibShowRepeatPassword).subscribe(o -> {
            if (!showRepeatPassword) {
                etRepeatPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                ibShowRepeatPassword.setImageResource(R.drawable.ic_visibility_off_blue_22dp);
                showRepeatPassword = true;
            } else {
                etRepeatPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                ibShowRepeatPassword.setImageResource(R.drawable.ic_visibility_blue_22dp);
                showRepeatPassword = false;
            }
        });

        etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().endsWith("knorrbremse.de")) {
                    etEmail.setText("@knorrbremse.de");
                    etEmail.setSelection(0);
                }
            }
        });

        etPersonalNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (etPersonalNumber.getError() != null && !TextUtils.isEmpty(editable))
                    etPersonalNumber.setError(null);
            }
        });
        btnRegister.requestFocus();
        etEmail.clearFocus();
        etEmail.setText("@knorrbremse.de");
        etEmail.setSelection(0);

        etRepeatPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(editable) || !editable.toString().equals(etPassword.getText().toString())) {
                    etRepeatPassword.setError(getResources().getString(R.string.passwords_must_match));
                } else {
                    etRepeatPassword.setError(null);
                }
            }
        });
        etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (etPassword.getError() != null && !TextUtils.isEmpty(editable))
                    etPassword.setError(null);
                if (!TextUtils.isEmpty(etRepeatPassword.getText()) && !editable.toString().equals(etRepeatPassword.getText().toString()))
                    etRepeatPassword.setError(getResources().getString(R.string.passwords_must_match));
            }
        });
    }
}
