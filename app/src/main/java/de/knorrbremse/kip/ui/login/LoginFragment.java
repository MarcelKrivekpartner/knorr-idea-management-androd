package de.knorrbremse.kip.ui.login;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import com.jakewharton.rxbinding2.view.RxView;

import androidx.navigation.Navigation;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.knorrbremse.kip.R;
import io.reactivex.functions.Consumer;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {

    @BindView(R.id.btn_register)
    Button btnRegister;


    @BindView(R.id.btn_login)
    Button btnLogin;

    @BindView(R.id.et_email)
    TextInputEditText etEmail;

    @BindView(R.id.et_password)
    TextInputEditText etPassword;

    @BindView(R.id.ib_show_password)
    ImageButton ibShowPassword;

    private boolean showPassword = false;

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @SuppressLint("CheckResult")

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        RxView.clicks(btnRegister).subscribe(o -> Navigation.findNavController(view).navigate(LoginFragmentDirections.toRegistration()));
        RxView.clicks(btnLogin).subscribe(o -> {
            if (etEmail.getText().toString().equals("@knorrbremse.de")) {
                etEmail.setError(getResources().getString(R.string.email_not_empty));
            }
            if (TextUtils.isEmpty(etPassword.getText())) {
                etPassword.setError(getResources().getString(R.string.password_not_empty));
            }
        });
        RxView.clicks(ibShowPassword).subscribe(o -> {
            if (!showPassword) {
                etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                ibShowPassword.setImageResource(R.drawable.ic_visibility_off_blue_22dp);
                showPassword = true;
            } else {
                etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                ibShowPassword.setImageResource(R.drawable.ic_visibility_blue_22dp);
                showPassword = false;
            }
        });
        etEmail.clearFocus();
        etEmail.setText("@knorrbremse.de");
        etEmail.setSelection(0);
        etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().endsWith("knorrbremse.de")) {
                    etEmail.setText("@knorrbremse.de");
                    etEmail.setSelection(0);
                }
            }
        });
        etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (etPassword.getError() != null && !TextUtils.isEmpty(editable))
                    etPassword.setError(null);
            }
        });
    }
}
